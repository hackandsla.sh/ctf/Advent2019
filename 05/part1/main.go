package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
)

func main() {
	rawInputs, err := ioutil.ReadFile("../inputs.txt")
	check(err)
	var stringInputs []string = strings.Split(string(rawInputs), ",")
	var inputs []int

	for _, value := range stringInputs {
		value = strings.Trim(value, "\r\n")
		i64, err := strconv.ParseInt(value, 10, 32)
		intRepresentation := int(i64)
		check(err)
		inputs = append(inputs, intRepresentation)
	}

	result := CalculateIntCode(inputs)
	fmt.Println(result[0])
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}

// CalculateIntCode takes in an IntCode and computes it according to IntCode rules:
// - 1: add the two numbers from the positions indicated by arguments 1 and 2, and put the result in the postition indicated by argument 3
// - 2: do the same as above, but multiply
// - 99: halt the program
func CalculateIntCode(code []int) []int {
	i := 0
	for {
		instruction := code[i]
		opcode := instruction % 100
		modesInt := instruction / 100

		var modes []int
		for i := 0; i < 3; i++ {
			modes = append(modes, modesInt%10)
			modesInt = modesInt / 10
		}

		if opcode == 99 {
			return code
		} else if opcode == 1 || opcode == 2 {
			params := getArguments(code, i, 2, modes)

			destinationPosition := code[i+3]
			var result int
			if opcode == 1 {
				result = params[0] + params[1]
			} else if opcode == 2 {
				result = params[0] * params[1]
			}

			code[destinationPosition] = result
			i += 4
		} else if opcode == 3 {

			reader := bufio.NewReader(os.Stdin)
			fmt.Print("Enter text: ")
			text, _ := reader.ReadString('\n')
			text = strings.Trim(text, "\n")
			input, err := strconv.Atoi(text)

			param := code[i+1]
			check(err)
			code[param] = input

			i += 2
		} else if opcode == 4 {
			params := getArguments(code, i, 1, modes)
			fmt.Println(params[0])

			i += 2
		} else if opcode == 5 {
			params := getArguments(code, i, 2, modes)

			if params[0] != 0 {
				i = params[1]
			} else {
				i += 3
			}

		} else if opcode == 6 {
			params := getArguments(code, i, 2, modes)

			if params[0] == 0 {
				i = params[1]
			} else {
				i += 3
			}

		} else if opcode == 7 {
			params := getArguments(code, i, 2, modes)
			destination := code[i+3]

			if params[0] < params[1] {
				code[destination] = 1
			} else {
				code[destination] = 0
			}

			i += 4

		} else if opcode == 8 {
			params := getArguments(code, i, 2, modes)
			destination := code[i+3]

			if params[0] == params[1] {
				code[destination] = 1
			} else {
				code[destination] = 0
			}
			i += 4

		} else {
			panic(fmt.Sprintf("opcode %d not recognized", opcode))
		}
	}
}

func splitIntToArray(num int) []int {
	var splitNum []int
	for i := num; i > 0; i /= 10 {
		splitNum = append(splitNum, i%10)
	}
	splitNum = reverseInts(splitNum)
	return splitNum
}

func reverseInts(input []int) []int {
	if len(input) == 0 {
		return input
	}
	return append(reverseInts(input[1:]), input[0])
}

func getArguments(code []int, position int, numArgs int, modes []int) []int {
	var ret []int
	for i := 0; i < numArgs; i++ {
		arg := code[position+1+i]
		if modes[i] == 0 {
			arg = code[arg]
		}
		ret = append(ret, arg)
	}
	return ret
}
