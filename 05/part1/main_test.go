package main

import (
	"reflect"
	"testing"
)

func TestCalculateIntCode(t *testing.T) {
	type args struct {
		code []int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			"Test 1",
			args{
				code: []int{1, 0, 0, 0, 99},
			},
			[]int{2, 0, 0, 0, 99},
		},
		{
			"Test 2",
			args{
				code: []int{2, 3, 0, 3, 99},
			},
			[]int{2, 3, 0, 6, 99},
		},
		{
			"Test 3",
			args{
				code: []int{2, 4, 4, 5, 99, 0},
			},
			[]int{2, 4, 4, 5, 99, 9801},
		},
		{
			"Test 4",
			args{
				code: []int{1, 1, 1, 4, 99, 5, 6, 0, 99},
			},
			[]int{30, 1, 1, 4, 2, 5, 6, 0, 99},
		},
		{
			"Test with immediate-mode values",
			args{
				code: []int{1001, 0, 4, 0, 99},
			},
			[]int{1005, 0, 4, 0, 99},
		},
		{
			"Test with immediate-mode values 2",
			args{
				code: []int{101, 0, 4, 0, 99},
			},
			[]int{99, 0, 4, 0, 99},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := CalculateIntCode(tt.args.code); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CalculateIntCode() = %v, want %v", got, tt.want)
			}
		})
	}
}
