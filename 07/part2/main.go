package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
	"time"
)

func main() {
	rawInputs, err := ioutil.ReadFile("../inputs.txt")
	check(err)
	var stringInputs []string = strings.Split(string(rawInputs), ",")
	var inputs []int

	for _, value := range stringInputs {
		value = strings.Trim(value, "\r\n")
		i64, err := strconv.ParseInt(value, 10, 32)
		intRepresentation := int(i64)
		check(err)
		inputs = append(inputs, intRepresentation)
	}

	perms := permutation([]int{5, 6, 7, 8, 9})
	highestValue := 0
	for _, perm := range perms {
		ampOutput := 0
		var inChans []chan int
		var outChans []chan int
		var doneChans []chan struct{}
		for i := 0; i < 5; i++ {
			inChans = append(inChans, make(chan int))
			doneChans = append(doneChans, make(chan struct{}))
		}
		for i := 0; i < 5; i++ {
			outChans = append(outChans, inChans[(i+1)%5])
		}

		finalChannel := outChans[4]
		finalDone := doneChans[4]
		tee := func(in chan int, done chan struct{}) (out chan int, out2 chan int) {
			out = make(chan int)
			out2 = make(chan int)
			go func() {
				for {
					select {
					case output := <-finalChannel:
						// fmt.Println(output)
						out2 <- output
						out <- output
					case <-done:
						return
					}
				}
			}()
			return out, out2
		}
		loopedChan, thrusterChan := tee(finalChannel, finalDone)
		outChans[4] = finalChannel
		inChans[0] = loopedChan

		for i := 0; i < 5; i++ {
			inChan := inChans[i]
			outChan := outChans[i]

			// Make a full copy of inputs. Apparently, using 'inputs' multiple times cause each goroutine to work on the same intcode
			var inputsCopy []int = make([]int, len(inputs))
			for j := 0; j < len(inputs); j++ {
				inputsCopy[j] = inputs[j]
			}
			go CalculateIntCode(inputsCopy, inChan, outChan, doneChans[i])
			inChans[i] <- perm[i]
		}

		inChans[0] <- 0
		<-finalDone
		time.Sleep(100 * time.Millisecond)
		ampOutput = outputArr[len(outputArr)-1]

		fmt.Printf("Permutation %v: %d\n", perm, ampOutput)
		if ampOutput > highestValue {
			highestValue = ampOutput
		}
	}

	fmt.Println("Highest Value: ", highestValue)
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}

// CalculateIntCode takes in an IntCode and computes it according to IntCode rules:
// - 1: add the two numbers from the positions indicated by arguments 1 and 2, and put the result in the postition indicated by argument 3
// - 2: do the same as above, but multiply
// - 99: halt the program
func CalculateIntCode(code []int, stdin <-chan int, stdout chan<- int, done chan struct{}) []int {
	i := 0
	for {
		instruction := code[i]
		opcode := instruction % 100
		modesInt := instruction / 100

		var modes []int
		for i := 0; i < 3; i++ {
			modes = append(modes, modesInt%10)
			modesInt = modesInt / 10
		}

		if opcode == 99 {
			select {
			case <-done:
			default:
				close(done)
			}
			return code
		} else if opcode == 1 || opcode == 2 {
			params := getArguments(code, i, 2, modes)

			destinationPosition := code[i+3]
			var result int
			if opcode == 1 {
				result = params[0] + params[1]
			} else if opcode == 2 {
				result = params[0] * params[1]
			}

			code[destinationPosition] = result
			i += 4
		} else if opcode == 3 {

			var input int

			select {
			case input = <-stdin:
			case <-done:
				return code
			}

			param := code[i+1]
			code[param] = input

			i += 2
		} else if opcode == 4 {
			params := getArguments(code, i, 1, modes)
			select {
			case stdout <- params[0]:
			case <-done:
				return code
			}
			// fmt.Println(params[0])

			i += 2
		} else if opcode == 5 {
			params := getArguments(code, i, 2, modes)

			if params[0] != 0 {
				i = params[1]
			} else {
				i += 3
			}

		} else if opcode == 6 {
			params := getArguments(code, i, 2, modes)

			if params[0] == 0 {
				i = params[1]
			} else {
				i += 3
			}

		} else if opcode == 7 {
			params := getArguments(code, i, 2, modes)
			destination := code[i+3]

			if params[0] < params[1] {
				code[destination] = 1
			} else {
				code[destination] = 0
			}

			i += 4

		} else if opcode == 8 {
			params := getArguments(code, i, 2, modes)
			destination := code[i+3]

			if params[0] == params[1] {
				code[destination] = 1
			} else {
				code[destination] = 0
			}
			i += 4

		} else {
			panic(fmt.Sprintf("opcode %d not recognized", opcode))
		}
	}
}

func splitIntToArray(num int) []int {
	var splitNum []int
	for i := num; i > 0; i /= 10 {
		splitNum = append(splitNum, i%10)
	}
	splitNum = reverseInts(splitNum)
	return splitNum
}

func reverseInts(input []int) []int {
	if len(input) == 0 {
		return input
	}
	return append(reverseInts(input[1:]), input[0])
}

func getArguments(code []int, position int, numArgs int, modes []int) []int {
	var ret []int
	for i := 0; i < numArgs; i++ {
		arg := code[position+1+i]
		if modes[i] == 0 {
			arg = code[arg]
		}
		ret = append(ret, arg)
	}
	return ret
}

func permutation(xs []int) (permuts [][]int) {
	var rc func([]int, int)
	rc = func(a []int, k int) {
		if k == len(a) {
			permuts = append(permuts, append([]int{}, a...))
		} else {
			for i := k; i < len(xs); i++ {
				a[k], a[i] = a[i], a[k]
				rc(a, k+1)
				a[k], a[i] = a[i], a[k]
			}
		}
	}
	rc(xs, 0)

	return permuts
}
