package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	rawInputs, err := ioutil.ReadFile("../inputs.txt")
	check(err)
	var stringInputs []string = strings.Split(string(rawInputs), ",")
	var inputs []int

	for _, value := range stringInputs {
		value = strings.Trim(value, "\r\n")
		i64, err := strconv.ParseInt(value, 10, 32)
		intRepresentation := int(i64)
		check(err)
		inputs = append(inputs, intRepresentation)
	}

	perms := permutation([]int{0, 1, 2, 3, 4})
	highestValue := 0
	for _, perm := range perms {
		ampOutput := 0
		for i := 0; i < 5; i++ {
			stdin := []int{
				perm[i],
				ampOutput,
				0,
			}

			_, stdout := CalculateIntCode(inputs, stdin)
			fmt.Printf("Permutation %v: %v\n", perm, stdout)
			ampOutput = stdout[0]
		}
		if ampOutput > highestValue {
			highestValue = ampOutput
		}
	}

	fmt.Println(highestValue)
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}

// CalculateIntCode takes in an IntCode and computes it according to IntCode rules:
// - 1: add the two numbers from the positions indicated by arguments 1 and 2, and put the result in the postition indicated by argument 3
// - 2: do the same as above, but multiply
// - 99: halt the program
func CalculateIntCode(code []int, stdin []int) ([]int, []int) {
	i := 0
	var stdout []int
	for {
		instruction := code[i]
		opcode := instruction % 100
		modesInt := instruction / 100

		var modes []int
		for i := 0; i < 3; i++ {
			modes = append(modes, modesInt%10)
			modesInt = modesInt / 10
		}

		if opcode == 99 {
			return code, stdout
		} else if opcode == 1 || opcode == 2 {
			params := getArguments(code, i, 2, modes)

			destinationPosition := code[i+3]
			var result int
			if opcode == 1 {
				result = params[0] + params[1]
			} else if opcode == 2 {
				result = params[0] * params[1]
			}

			code[destinationPosition] = result
			i += 4
		} else if opcode == 3 {

			var input int
			input, stdin = stdin[0], stdin[1:len(stdin)]

			param := code[i+1]
			code[param] = input

			i += 2
		} else if opcode == 4 {
			params := getArguments(code, i, 1, modes)
			stdout = append(stdout, params[0])
			// fmt.Println(params[0])

			i += 2
		} else if opcode == 5 {
			params := getArguments(code, i, 2, modes)

			if params[0] != 0 {
				i = params[1]
			} else {
				i += 3
			}

		} else if opcode == 6 {
			params := getArguments(code, i, 2, modes)

			if params[0] == 0 {
				i = params[1]
			} else {
				i += 3
			}

		} else if opcode == 7 {
			params := getArguments(code, i, 2, modes)
			destination := code[i+3]

			if params[0] < params[1] {
				code[destination] = 1
			} else {
				code[destination] = 0
			}

			i += 4

		} else if opcode == 8 {
			params := getArguments(code, i, 2, modes)
			destination := code[i+3]

			if params[0] == params[1] {
				code[destination] = 1
			} else {
				code[destination] = 0
			}
			i += 4

		} else {
			panic(fmt.Sprintf("opcode %d not recognized", opcode))
		}
	}
}

func splitIntToArray(num int) []int {
	var splitNum []int
	for i := num; i > 0; i /= 10 {
		splitNum = append(splitNum, i%10)
	}
	splitNum = reverseInts(splitNum)
	return splitNum
}

func reverseInts(input []int) []int {
	if len(input) == 0 {
		return input
	}
	return append(reverseInts(input[1:]), input[0])
}

func getArguments(code []int, position int, numArgs int, modes []int) []int {
	var ret []int
	for i := 0; i < numArgs; i++ {
		arg := code[position+1+i]
		if modes[i] == 0 {
			arg = code[arg]
		}
		ret = append(ret, arg)
	}
	return ret
}

func permutation(xs []int) (permuts [][]int) {
	var rc func([]int, int)
	rc = func(a []int, k int) {
		if k == len(a) {
			permuts = append(permuts, append([]int{}, a...))
		} else {
			for i := k; i < len(xs); i++ {
				a[k], a[i] = a[i], a[k]
				rc(a, k+1)
				a[k], a[i] = a[i], a[k]
			}
		}
	}
	rc(xs, 0)

	return permuts
}
