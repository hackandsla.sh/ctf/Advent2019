package main

import (
	"reflect"
	"testing"
)

func TestCalculateIntCode(t *testing.T) {
	type args struct {
		code  []int
		stdin []int
	}
	tests := []struct {
		name  string
		args  args
		want  []int
		want1 []int
	}{
		{
			name: "Test 1",
			args: args{
				code: []int{1, 0, 0, 0, 99},
			},
			want: []int{2, 0, 0, 0, 99},
		},
		{
			name: "Test 2",
			args: args{
				code: []int{2, 3, 0, 3, 99},
			},
			want: []int{2, 3, 0, 6, 99},
		},
		{
			name: "Test 3",
			args: args{
				code: []int{2, 4, 4, 5, 99, 0},
			},
			want: []int{2, 4, 4, 5, 99, 9801},
		},
		{
			name: "Test 4",
			args: args{
				code: []int{1, 1, 1, 4, 99, 5, 6, 0, 99},
			},
			want: []int{30, 1, 1, 4, 2, 5, 6, 0, 99},
		},
		{
			name: "Test with immediate-mode values",
			args: args{
				code: []int{1001, 0, 4, 0, 99},
			},
			want: []int{1005, 0, 4, 0, 99},
		},
		{
			name: "Test with immediate-mode values 2",
			args: args{
				code: []int{101, 0, 4, 0, 99},
			},
			want: []int{99, 0, 4, 0, 99},
		},
		{
			name: "Test with output",
			args: args{
				code: []int{3, 21, 1008, 21, 8, 20, 1005, 20, 22, 107, 8, 21, 20, 1006, 20, 31,
					1106, 0, 36, 98, 0, 0, 1002, 21, 125, 20, 4, 20, 1105, 1, 46, 104,
					999, 1105, 1, 46, 1101, 1000, 1, 20, 4, 20, 1105, 1, 46, 98, 99,
				},
				stdin: []int{1},
			},
			want:  []int{3, 21, 1008, 21, 8, 20, 1005, 20, 22, 107, 8, 21, 20, 1006, 20, 31, 1106, 0, 36, 98, 0, 1, 1002, 21, 125, 20, 4, 20, 1105, 1, 46, 104, 999, 1105, 1, 46, 1101, 1000, 1, 20, 4, 20, 1105, 1, 46, 98, 99},
			want1: []int{999},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1 := CalculateIntCode(tt.args.code, tt.args.stdin)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CalculateIntCode() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("CalculateIntCode() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}
