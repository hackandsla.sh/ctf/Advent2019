package main

import (
	"reflect"
	"testing"
)

func Test_splitIntToArray(t *testing.T) {
	type args struct {
		num int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			name: "Test 1",
			args: args{
				num: 123456,
			},
			want: []int{1, 2, 3, 4, 5, 6},
		},
		{
			name: "Test 2",
			args: args{
				num: 4444,
			},
			want: []int{4, 4, 4, 4},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := splitIntToArray(tt.args.num); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("splitIntToArray() = %v, want %v", got, tt.want)
			}
		})
	}
}
