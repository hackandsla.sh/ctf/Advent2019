package main

import "fmt"

func main() {
	start := 134564
	end := 585159

	var answers []int

outer:
	for guess := start; guess < end; guess++ {
		splitGuess := splitIntToArray(guess)
		hasDuplicate := false
		for i := 1; i < len(splitGuess); i++ {
			if splitGuess[i] < splitGuess[i-1] {
				continue outer
			}
			if splitGuess[i] == splitGuess[i-1] {
				hasDuplicate = true
			}
		}

		if hasDuplicate {
			answers = append(answers, guess)
		}
	}

	fmt.Println(len(answers))
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func splitIntToArray(num int) []int {
	var splitNum []int
	for i := num; i > 0; i /= 10 {
		splitNum = append(splitNum, i%10)
	}
	splitNum = reverseInts(splitNum)
	return splitNum
}

func reverseInts(input []int) []int {
	if len(input) == 0 {
		return input
	}
	return append(reverseInts(input[1:]), input[0])
}
