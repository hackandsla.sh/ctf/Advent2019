package main

import (
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	rawInputs, err := ioutil.ReadFile("../inputs.txt")
	check(err)
	var stringInputs []string = strings.Split(string(rawInputs), "\n")
	var wireVectors [][]string
	for _, value := range stringInputs {
		split := strings.Split(value, ",")
		wireVectors = append(wireVectors, split)
	}
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func findShortestIntersection(wire1, wire2 []string) int {
	vertices1 := getVertices(wire1)
	vertices2 := getVertices(wire2)
	return 0
}

func getVertices(instructions []string) []Coordinate {
	wireCoords := []Coordinate{Coordinate{0, 0}}
	for _, instruction := range instructions {
		direction := instruction[0:1]
		magnitudeString := instruction[1:]
		if len(magnitudeString) == 0 {
			continue
		}
		magnitude, err := strconv.Atoi(magnitudeString)
		check(err)

		lastCoords := wireCoords[len(wireCoords)-1]
		x := lastCoords.X
		y := lastCoords.Y
		if direction == "U" {
			y += magnitude
		} else if direction == "D" {
			y -= magnitude
		} else if direction == "L" {
			x -= magnitude
		} else if direction == "R" {
			x += magnitude
		}

		newCoords := Coordinate{
			X: x,
			Y: y,
		}
		wireCoords = append(wireCoords, newCoords)
	}
	return wireCoords
}

func getIntersections(wire1, wire2 []Coordinate) []Coordinate {
	var ret []Coordinate
	for i := 1; i < len(wire1); i++ {
		segment1 := Segment{
			start: wire1[i-1],
			stop:  wire1[i],
		}
		for j := 1; j < len(wire2); j++ {
			segment2 := Segment{
				start: wire2[j-1],
				stop:  wire2[j],
			}
			var xCoord, yCoord int

			if segment1.start.X == segment1.stop.X && withinRange(segment1.start.X, segment2.start.X, segment2.stop.X) {
				xCoord = segment1.start.X
			}

		}
	}
}

func withinRange(i, j, k int) bool {
	if (i > j && i < k) ||
		(i < j && i > k) {
		return true
	}
	return false
}

// Coordinate indicates a point on a 2D field
type Coordinate struct {
	X int
	Y int
}

type Segment struct {
	start Coordinate
	stop  Coordinate
}
