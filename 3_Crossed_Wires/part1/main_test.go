package main

import (
	"reflect"
	"testing"
)

func Test_findShortestIntersection(t *testing.T) {
	type args struct {
		wires [][]string
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "Test 1",
			args: args{
				wires: [][]string{[]string{"R75", "D30", "R83", "U83", "L12", "D49", "R71", "U7", "L72"}, []string{"U62", "R66", "U55", "R34", "D71", "R55", "D58", "R83"}},
			},
			want: 159,
		},
		{
			name: "Test 2",
			args: args{
				wires: [][]string{[]string{"R98", "U47", "R26", "D63", "R33", "U87", "L62", "D20", "R33", "U53", "R51"}, []string{"U98", "R91", "D20", "R16", "D67", "R40", "U7", "R15", "U6", "R7"}},
			},
			want: 135,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := findShortestIntersection(tt.args.wires); got != tt.want {
				t.Errorf("findShortestIntersection() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_getVertices(t *testing.T) {
	type args struct {
		instructions []string
	}
	tests := []struct {
		name string
		args args
		want []Coordinate
	}{
		{
			name: "Test 1",
			args: args{
				instructions: []string{"U10", "R20", "D30", "L40"},
			},
			want: []Coordinate{
				Coordinate{0, 0},
				Coordinate{0, 10},
				Coordinate{20, 10},
				Coordinate{20, -20},
				Coordinate{-20, -20},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getVertices(tt.args.instructions); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("getVertices() = %v, want %v", got, tt.want)
			}
		})
	}
}
