package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	rawInputs, err := ioutil.ReadFile("../inputs.txt")
	check(err)
	var stringInputs []string = strings.Split(string(rawInputs), "\r\n")
	var inputs []int64

	for _, value := range stringInputs {
		intRepresentation, err := strconv.ParseInt(value, 10, 64)
		check(err)
		inputs = append(inputs, intRepresentation)
	}

	var total int64 = 0
	for _, value := range inputs {
		fuelNeeded := (value / 3) - 2
		total += fuelNeeded
	}
	fmt.Println(total)
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}
