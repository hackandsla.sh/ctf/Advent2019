package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	rawInputs, err := ioutil.ReadFile("../inputs.txt")
	check(err)
	var stringInputs []string = strings.Split(string(rawInputs), ",")
	var inputs []int

	for _, value := range stringInputs {
		value = strings.Trim(value, "\r\n")
		i64, err := strconv.ParseInt(value, 10, 32)
		intRepresentation := int(i64)
		check(err)
		inputs = append(inputs, intRepresentation)
	}

	var noun int
	var verb int
outer:
	for noun = 0; noun < 100; noun++ {
		for verb = 0; verb < 100; verb++ {
			currentInputs := make([]int, len(inputs))
			copy(currentInputs, inputs)

			currentInputs[1] = noun
			currentInputs[2] = verb
			output := CalculateIntCode(currentInputs)
			if output != nil {
				result := output[0]

				if result == 19690720 {
					break outer
				}
			}
		}
	}

	answer := 100*noun + verb
	fmt.Println(answer)
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}

// CalculateIntCode takes in an IntCode and computes it according to IntCode rules:
// - 1: add the two numbers from the positions indicated by arguments 1 and 2, and put the result in the postition indicated by argument 3
// - 2: do the same as above, but multiply
// - 99: halt the program
func CalculateIntCode(code []int) []int {
	i := 0
	for {
		if i >= len(code) || code[i] == 99 {
			return code
		}

		if code[i] == 1 || code[i] == 2 {
			operandPosition1 := code[i+1]
			operandPosition2 := code[i+2]
			operand1 := code[operandPosition1]
			operand2 := code[operandPosition2]

			destinationPosition := code[i+3]
			var result int
			if code[i] == 1 {
				result = operand1 + operand2
			} else if code[i] == 2 {
				result = operand1 * operand2
			}

			code[destinationPosition] = result
		}

		i += 4
	}
}
