package main

import (
	"fmt"
	"io/ioutil"
	"strings"
)

func main() {
	rawInputs, err := ioutil.ReadFile("../inputs.txt")
	check(err)
	var stringInputs []string = strings.Split(string(rawInputs), "\n")
	var inputs []string

	for _, value := range stringInputs {
		value := strings.Trim(value, "\r\n")
		inputs = append(inputs, value)
	}

	fmt.Println(getOrbitDiff(inputs, "YOU", "SAN"))
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func getOrbitDiff(inputs []string, start, end string) int {
	// a mapping of names to parents
	var nodes map[string]string = make(map[string]string)

	for _, value := range inputs {
		splitValue := strings.Split(value, ")")
		parent := splitValue[0]
		orbitter := splitValue[1]

		nodes[orbitter] = parent
	}

	myPath := getPath(nodes, "YOU")
	sanPath := getPath(nodes, "SAN")

	i := 0
	for {
		if myPath[i] != sanPath[i] {
			break
		}
		i++
	}
	myPathRemaining := len(myPath[i:])
	sanPathRemaining := len(sanPath[i:])
	return myPathRemaining + sanPathRemaining
}

func getPath(nodes map[string]string, target string) []string {
	var ret []string

	for {
		next, ok := nodes[target]
		if ok {
			target = next
			// Push node to the top
			ret = append([]string{next}, ret...)
		} else {
			break
		}
	}

	return ret
}
