package main

import (
	"reflect"
	"testing"
)

func Test_getOrbitDiff(t *testing.T) {
	type args struct {
		inputs []string
		start  string
		end    string
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "Test 1",
			args: args{
				inputs: []string{
					"COM)B",
					"B)C",
					"C)D",
					"D)E",
					"E)F",
					"B)G",
					"G)H",
					"D)I",
					"E)J",
					"J)K",
					"K)L",
					"K)YOU",
					"I)SAN",
				},
				start: "YOU",
				end:   "SAN",
			},
			want: 4,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getOrbitDiff(tt.args.inputs, tt.args.start, tt.args.end); got != tt.want {
				t.Errorf("getOrbitDiff() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_getPath(t *testing.T) {
	type args struct {
		nodes  map[string]string
		target string
	}
	tests := []struct {
		name string
		args args
		want []string
	}{
		{
			name: "Test 1",
			args: args{
				nodes: map[string]string{
					"B":   "COM",
					"C":   "B",
					"D":   "C",
					"FOO": "C",
				},
				target: "FOO",
			},
			want: []string{
				"COM",
				"B",
				"C",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getPath(tt.args.nodes, tt.args.target); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("getPath() = %v, want %v", got, tt.want)
			}
		})
	}
}
