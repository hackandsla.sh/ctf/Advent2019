package main

import (
	"fmt"
	"io/ioutil"
	"strings"
)

func main() {
	rawInputs, err := ioutil.ReadFile("../inputs.txt")
	check(err)
	var stringInputs []string = strings.Split(string(rawInputs), "\n")
	var inputs []string

	for _, value := range stringInputs {
		value := strings.Trim(value, "\r\n")
		inputs = append(inputs, value)
	}

	fmt.Println(getOrbitChecksum(inputs))
}

type OrbitNode struct {
	Name     string
	Children []OrbitNode
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func getOrbitChecksum(inputs []string) int {
	// a mapping of names to parents
	var nodes map[string]string = make(map[string]string)

	for _, value := range inputs {
		splitValue := strings.Split(value, ")")
		parent := splitValue[0]
		orbitter := splitValue[1]

		nodes[orbitter] = parent
	}

	count := 0
	for _, parent := range nodes {
		count++
		for {
			next, ok := nodes[parent]
			if ok {
				parent = next
				count++
			} else {
				break
			}
		}
	}
	return count
}
