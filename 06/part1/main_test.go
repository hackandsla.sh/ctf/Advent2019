package main

import (
	"testing"
)

func Test_getOrbitChecksum(t *testing.T) {
	type args struct {
		inputs []string
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "Test 1",
			args: args{
				inputs: []string{
					"COM)B",
					"B)C",
					"C)D",
					"D)E",
					"E)F",
					"B)G",
					"G)H",
					"D)I",
					"E)J",
					"J)K",
					"K)L",
				},
			},
			want: 42,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getOrbitChecksum(tt.args.inputs); got != tt.want {
				t.Errorf("getOrbitChecksum() = %v, want %v", got, tt.want)
			}
		})
	}
}
