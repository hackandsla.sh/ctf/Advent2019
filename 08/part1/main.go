package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	rawInputs, err := ioutil.ReadFile("../inputs.txt")
	stringInput := strings.Trim(string(rawInputs), "\r\n")
	check(err)
	var stringInputs []string = strings.Split(stringInput, "")
	var inputs []int

	fmt.Println(len(stringInputs))
	for _, value := range stringInputs {
		int64Representation, err := strconv.ParseInt(value, 10, 32)
		intRepresentation := int(int64Representation)
		check(err)
		inputs = append(inputs, intRepresentation)
	}

	width := 25
	height := 6

	var layers [][]int = make([][]int, 100)
	for i := 0; i < len(inputs); i++ {
		layer := i / (width * height)
		layers[layer] = append(layers[layer], inputs[i])
	}

	minNumZeros := 9999999999
	minNumZerosLayer := -1
	for i, layer := range layers {
		numZeroes := 0
		for _, pixel := range layer {
			if pixel == 0 {
				numZeroes++
			}
		}
		if numZeroes < minNumZeros {
			minNumZeros = numZeroes
			minNumZerosLayer = i
		}
	}

	numOnes := 0
	numTwos := 0
	for _, pixel := range layers[minNumZerosLayer] {
		if pixel == 1 {
			numOnes++
		} else if pixel == 2 {
			numTwos++
		}
	}
	fmt.Println(numOnes * numTwos)
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}
