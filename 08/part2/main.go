package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	rawInputs, err := ioutil.ReadFile("../inputs.txt")
	stringInput := strings.Trim(string(rawInputs), "\r\n")
	check(err)
	var stringInputs []string = strings.Split(stringInput, "")
	var inputs []int

	for _, value := range stringInputs {
		int64Representation, err := strconv.ParseInt(value, 10, 32)
		intRepresentation := int(int64Representation)
		check(err)
		inputs = append(inputs, intRepresentation)
	}

	const width = 25
	const height = 6

	var layers [][height][width]int

	i := 0
	l := 0
outer:
	for {
		layers = append(layers, [height][width]int{})
		for h := 0; h < height; h++ {
			for w := 0; w < width; w++ {
				fmt.Println(l)
				layers[l][h][w] = inputs[i]
				i++
				if i >= len(inputs) {
					break outer
				}
			}
		}
		l++
	}

	// var finalLayer [height][width]int
	for h := 0; h < height; h++ {
		for w := 0; w < width; w++ {
			for layer := range layers {
				pixel := layers[layer][h][w]
				if pixel == 0 {
					fmt.Print("X")
					break
				} else if pixel == 1 {
					fmt.Print(" ")
					break
				}
			}
		}
		fmt.Println()
	}

}

func check(err error) {
	if err != nil {
		panic(err)
	}
}
